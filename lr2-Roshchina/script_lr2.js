var gl;
function initGL(canvas) {
    try {
        gl = canvas.getContext("experimental-webgl");
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;
    } catch (e) { }
    if (!gl) { alert("Не поддерживается WebGL"); }
}
function getShader(gl, id) {
    var shaderScript = document.getElementById(id);
    if (!shaderScript) { return null; }
    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
        if (k.nodeType == 3) { str += k.textContent; }
        k = k.nextSibling;
    }
    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else { return null; }
    gl.shaderSource(shader, str);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}
var shaderProgram;
function initShaders() {
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    var fragmentShader1 = getShader(gl, "shader-fs-01");
    var fragmentShader2 = getShader(gl, "shader-fs-02");
    var fragmentShader3 = getShader(gl, "shader-fs-03");
    var fragmentShader4 = getShader(gl, "shader-fs-04");
    var fragmentShader5 = getShader(gl, "shader-fs-05");
    var fragmentShader6 = getShader(gl, "shader-fs-06");

    var selDrawType = document.getElementById("selDrawType").value;
    var drawType = eval(selDrawType);

    shaderProgram = gl.createProgram();

    gl.attachShader(shaderProgram, vertexShader);
    
    switch (drawType) {
        case gl.LINES:
            gl.attachShader(shaderProgram, fragmentShader1);
            break;
        case gl.LINE_STRIP:
            gl.attachShader(shaderProgram, fragmentShader2);
            break;
        case gl.LINE_LOOP:
            gl.attachShader(shaderProgram, fragmentShader3);
            break;
        case gl.TRIANGLES:
            gl.attachShader(shaderProgram, fragmentShader4);
            break;
        case gl.TRIANGLE_STRIP:
            gl.attachShader(shaderProgram, fragmentShader5);
            break;
        case gl.TRIANGLE_FAN:
            gl.attachShader(shaderProgram, fragmentShader6);
            break;
        default:
            gl.attachShader(shaderProgram, fragmentShader);
    }

    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Шейдеры не инициализируются");
    }

    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram,
        "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram,
        "uPMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram,
        "uMVMatrix");
}
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

var vertexBuffer;
function initBuffers() {
    vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

    vertices = [
        12, 0, 0,
        7, 1, 0,
        6, 1, 0,
        2, 1, 0,
        1, 3, 0,
        2, 5, 0,
        4, 5, 0,
        4, 9, 0,
        8, 9, 0,
        8, 7, 0,
        7, 7, 0,
        8, 5, 0,
    ];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices),
        gl.STATIC_DRAW);
    vertexBuffer.itemSize = 3;
    vertexBuffer.numItems = 12;
}
function drawScene() {
    initShaders();
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    mat4.perspective(pMatrix, 45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0);
    mat4.identity(mvMatrix);
    mat4.translate(mvMatrix, mvMatrix, [-12.0, -12, -24.0]);
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
        vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
    setMatrixUniforms();
    var selDrawType = document.getElementById("selDrawType").value;
    var drawType = eval(selDrawType);

    gl.drawArrays(drawType, 0, vertexBuffer.numItems);
}
function webGLStart() {
    var canvas = document.getElementById("canvas");
    initGL(canvas);
    initShaders();
    initBuffers();
    gl.clearColor(0.2, 0.2, 0.2, 1.0);
    drawScene();
}

// глобальные переменные
var container, camera, controls, scene, renderer, light;
var Cube;
// начинаем рисовать после полной загрузки страницы
window.onload = function () {
    init();
    animate();
}
function init() {
    scene = new THREE.Scene(); //создаем сцену
    AddCamera(0, 300, 500); //добавляем камеру
    AddLight(0, 0, 500); //устанавливаем белый свет

    //создаем рендерер
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0xffffff);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container = document.getElementById('MyWebGLApp');
    container.appendChild(renderer.domElement);

    var axes = new THREE.AxesHelper(150);
    axes.position.set(0, 0, 0);
    scene.add(axes);
    var gridXY = new THREE.GridHelper(100, 20, new THREE.Color(0x0000ff), new THREE.Color(0x0000ff));
    gridXY.position.set(50, 50, 0);
    gridXY.rotation.x = Math.PI / 2;
    scene.add(gridXY);

    var vectors = [
        new THREE.Vector2(0, 12),
        new THREE.Vector2(1, 2),
        new THREE.Vector2(2, 7),
        new THREE.Vector2(5, 1),
        new THREE.Vector2(6, 5),
        new THREE.Vector2(6, 9),
        new THREE.Vector2(7, 0),
        new THREE.Vector2(7, 10),
        new THREE.Vector2(8, 9),
        new THREE.Vector2(8, 9),
        new THREE.Vector2(12, 4),
        new THREE.Vector2(12, 5)
    ];

    var splineGeometry = new THREE.Geometry;
    var splineMaterial = new THREE.LineBasicMaterial({ color: 0xcc0000, linewidth: 2 });

    var spline = new THREE.SplineCurve(vectors);

    for (var i = 0; i <= 1; i += 0.01) {
        var x = spline.getPoint(i).x * 5;
        var y = spline.getPoint(i).y * 5;
        var vec = new THREE.Vector3(x, y, 6);
        splineGeometry.vertices.push(vec);
    }

    var quadGeometry = new THREE.Geometry;
    var quadMaterial = new THREE.LineBasicMaterial({ color: 0x00cc00, linewidth: 2 });

    for (var v = 0; v < vectors.length - 2; v += 2) {
        var quad = new THREE.QuadraticBezierCurve(vectors[v], vectors[v+1], vectors[v+2]);
        for (var i = 0; i <= 1; i += 0.01) {
            var x = quad.getPoint(i).x * 5;
            var y = quad.getPoint(i).y * 5;
            var vec = new THREE.Vector3(x, y, 12);
            quadGeometry.vertices.push(vec);
        }
        console.log(quad);
    }

    var cubicGeometry = new THREE.Geometry;
    var cubicMaterial = new THREE.LineBasicMaterial({ color: 0x0000cc, linewidth: 2 });

    for (var v = 0; v < vectors.length - 3; v += 3) {
        var cubic = new THREE.CubicBezierCurve(vectors[v], vectors[v+1], vectors[v+2], vectors[v+3]);
        for (var i = 0; i <= 1; i += 0.01) {
            var x = cubic.getPoint(i).x * 5;
            var y = cubic.getPoint(i).y * 5;
            var vec = new THREE.Vector3(x, y, 0);
            cubicGeometry.vertices.push(vec);
        }
        console.log(quad);
    }

    scene.add(new THREE.Line(splineGeometry, splineMaterial));
    scene.add(new THREE.Line(quadGeometry, quadMaterial));
    scene.add(new THREE.Line(cubicGeometry, cubicMaterial));
}
function animate() {
    requestAnimationFrame(animate);
    render();
}
function render() {
    controls.update();
    renderer.render(scene, camera);
}
function AddCamera(X, Y, Z) {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth /
        window.innerHeight, 1, 10000);
    camera.position.set(X, Y, Z);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 2;
    controls.noZoom = false;
    controls.zoomSpeed = 1.2;
    controls.staticMoving = true;
}
function AddLight(X, Y, Z) {
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(X, Y, Z);
    scene.add(light);
}
// глобальные переменные
var container, camera, controls, scene, renderer, light;
var Cube;
// начинаем рисовать после полной загрузки страницы
window.onload = function () {
    init();
    animate();
}
function init() {
    scene = new THREE.Scene(); //создаем сцену
    AddCamera(0, 300, 500); //добавляем камеру
    AddLight(0, 0, 500); //устанавливаем белый свет

    //создаем рендерер
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0xffffff);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container = document.getElementById('MyWebGLApp');
    container.appendChild(renderer.domElement);

    var axes = new THREE.AxesHelper(150);
    axes.position.set(0, 0, 0);
    scene.add(axes);
    var gridXY = new THREE.GridHelper(100, 20, new THREE.Color(0x0000ff), new THREE.Color(0x0000ff));
    gridXY.position.set(50, 50, 0);
    gridXY.rotation.x = Math.PI / 2;
    scene.add(gridXY);

    var points = [
        0, 12,
        1, 2,
        2, 7,
        5, 1,
        6, 5,
        6, 9,
        7, 0,
        7, 10,
        8, 9,
        8, 9,
        12, 4,
        12, 5
    ];

    var shape = new THREE.Shape();

    for (var t = 0; t < points.length; t += 2) {
        shape.lineTo(5*points[t], 5*points[t + 1], 0);
    }

    var geometry = new THREE.ShapeGeometry(shape );
    var material = new THREE.LineBasicMaterial({ color: 0xcc0000, linewidth: 2 });

    scene.add(new THREE.Line(geometry, material));
}
function animate() {
    requestAnimationFrame(animate);
    render();
}
function render() {
    controls.update();
    renderer.render(scene, camera);
}
function AddCamera(X, Y, Z) {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth /
        window.innerHeight, 1, 10000);
    camera.position.set(X, Y, Z);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 2;
    controls.noZoom = false;
    controls.zoomSpeed = 1.2;
    controls.staticMoving = true;
}
function AddLight(X, Y, Z) {
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(X, Y, Z);
    scene.add(light);
}
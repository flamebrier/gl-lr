// глобальные переменные
var container, camera, controls, scene, renderer, light;
var Cube;
// начинаем рисовать после полной загрузки страницы
window.onload = function () {
    init();
    animate();
}
function init() {
    scene = new THREE.Scene(); //создаем сцену
    AddCamera(0, 300, 500); //добавляем камеру
    AddLight(0, 0, 500); //устанавливаем белый свет

    //создаем рендерер
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0xffffff);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container = document.getElementById('MyWebGLApp');
    container.appendChild(renderer.domElement);

    var axes = new THREE.AxesHelper(150);
    axes.position.set(0, 0, 0);
    scene.add(axes);
    var gridXY = new THREE.GridHelper(100, 20, new THREE.Color(0x0000ff), new THREE.Color(0x0000ff));
    gridXY.position.set(50, 50, 0);
    gridXY.rotation.x = Math.PI / 2;
    scene.add(gridXY);

    var vectors = [
        new THREE.Vector3(0, 12, 0),
        new THREE.Vector3(1, 2, 0),
        new THREE.Vector3(2, 7, 0),
        new THREE.Vector3(5, 1, 0),
        new THREE.Vector3(6, 5, 0),
        new THREE.Vector3(6, 9, 0),
        new THREE.Vector3(7, 0, 0),
        new THREE.Vector3(7, 10, 0),
        new THREE.Vector3(8, 9, 0),
        new THREE.Vector3(8, 9, 0),
        new THREE.Vector3(12, 4, 0),
        new THREE.Vector3(12, 5, 0)
    ];

    var vectorsX5 = [];

    vectors.forEach(function (v) {
        vectorsX5.push(new THREE.Vector3(v.x * 5, v.y * 5, v.z * 5));
    });

    var spline = new THREE.CatmullRomCurve3(vectorsX5);
    var splineGeometry = new THREE.TubeGeometry(spline, 128, 2, 12);
    var splineMaterial = new THREE.MeshPhongMaterial({
        color: 0xcc0000, specular: 0xd53e07,
        emissive: 0x000000, shininess: 40, flatShading: true, blending:
            THREE.NormalBlending, depthTest: true
    });

    var quadList = [];
    for (var v = 0; v < vectorsX5.length - 2; v += 2) {
        var quad = new THREE.QuadraticBezierCurve(vectorsX5[v], vectorsX5[v+1], vectorsX5[v+2]);
        for (var i = 0; i <= 1; i += 0.01) {
            var x = quad.getPoint(i).x;
            var y = quad.getPoint(i).y;
            var vec = new THREE.Vector3(x, y, 6);
            quadList.push(vec);
        }
    }
    var quad = new THREE.CatmullRomCurve3(quadList);
    var quadGeometry = new THREE.TubeGeometry(quad, 128, 2, 12);
    var quadMaterial = new THREE.MeshPhongMaterial({
        color: 0x00cc00, specular: 0xd53e07,
        emissive: 0x000000, shininess: 40, flatShading: true, blending:
            THREE.NormalBlending, depthTest: true
    });

    var cubicList = [];
    for (var v = 0; v < vectorsX5.length - 3; v += 3) {
        var cubic = new THREE.CubicBezierCurve(vectorsX5[v], vectorsX5[v+1], vectorsX5[v+2], vectorsX5[v+3]);
        for (var i = 0; i <= 1; i += 0.01) {
            var x = cubic.getPoint(i).x;
            var y = cubic.getPoint(i).y;
            var vec = new THREE.Vector3(x, y, 12);
            cubicList.push(vec);
        }
    }
    var cubic = new THREE.CatmullRomCurve3(cubicList);
    var cubicGeometry = new THREE.TubeGeometry(cubic, 128, 2, 12);
    var cubicMaterial = new THREE.MeshPhongMaterial({
        color: 0x0000cc, specular: 0xd53e07,
        emissive: 0x000000, shininess: 40, flatShading: true, blending:
            THREE.NormalBlending, depthTest: true
    });

    scene.add(new THREE.Mesh(splineGeometry, splineMaterial));
    scene.add(new THREE.Mesh(quadGeometry, quadMaterial));
    scene.add(new THREE.Mesh(cubicGeometry, cubicMaterial));

}
function animate() {
    requestAnimationFrame(animate);
    render();
}
function render() {
    controls.update();
    renderer.render(scene, camera);
}
function AddCamera(X, Y, Z) {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth /
        window.innerHeight, 1, 10000);
    camera.position.set(X, Y, Z);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 2;
    controls.noZoom = false;
    controls.zoomSpeed = 1.2;
    controls.staticMoving = true;
}
function AddLight(X, Y, Z) {
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(X, Y, Z);
    scene.add(light);
}
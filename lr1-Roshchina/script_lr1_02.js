// глобальные переменные
var container, camera, controls, scene, renderer, light;
var Cube;
// начинаем рисовать после полной загрузки страницы
window.onload = function () {
    init();
    animate();
}
function init() {
    scene = new THREE.Scene(); //создаем сцену
    AddCamera(0, 300, 500); //добавляем камеру
    AddLight(0, 0, 500); //устанавливаем белый свет

    //создаем рендерер
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0xffffff);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container = document.getElementById('MyWebGLApp');
    container.appendChild(renderer.domElement);

    var length = 200, width = 150;
    var innerLength = 50, innerWidth = 30;
    var x = (length - innerLength) / 2;
    var y = (width - innerWidth) / 2;

    var shape = new THREE.Shape();
    shape.moveTo(0, 0);
    shape.lineTo(0, width);
    shape.lineTo(length, width);
    shape.lineTo(length, 0);
    shape.lineTo(x, 0);
    shape.lineTo(x, y);
    shape.lineTo(x + innerLength, y);
    shape.lineTo(x + innerLength, y + innerWidth);
    shape.lineTo(x, y + innerWidth);
    shape.lineTo(x, 0);
    shape.lineTo(0, 0);

    var shape2 = new THREE.Shape();
    shape2.moveTo(x, y);
    shape2.lineTo(x, y + innerWidth);
    shape2.lineTo(x + innerLength, y + innerWidth);
    shape2.lineTo(x + innerLength, y);
    shape2.lineTo(x, y);

    var extrudeSettings = {
        steps: 1,
        depth: 30,
        bevelEnabled: false,
    };

    var geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
    var material = new THREE.MeshPhongMaterial({ color: 0x00afaf });
    Cube = new THREE.Mesh(geometry, material);

    Cube.position.z = -100;
    Cube.rotation.z = Math.PI / 6;
    scene.add(Cube);
}
function animate() {
    requestAnimationFrame(animate);
    render();
}
function render() {
    Cube.position.x = Cube.position.x + 0; // +1 - куб движется
    Cube.rotation.y = Cube.rotation.y + 0.01; //и вращается вокруг оси
    controls.update();
    renderer.render(scene, camera);
}
function AddCamera(X, Y, Z) {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth /
        window.innerHeight, 1, 10000);
    camera.position.set(X, Y, Z);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 2;
    controls.noZoom = false;
    controls.zoomSpeed = 1.2;
    controls.staticMoving = true;
}
function AddLight(X, Y, Z) {
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(X, Y, Z);
    scene.add(light);
}
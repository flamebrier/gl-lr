// глобальные переменные
var container, camera, controls, scene, renderer, light;
// начинаем рисовать после полной загрузки страницы
window.onload = function () {
    init();
    animate();
}

function init() {
    scene = new THREE.Scene(); //создаем сцену 
    AddCamera(0, 0, 700); //добавляем камеру
    AddLight(0, 0, 500); //устанавливаем белый свет

    //создаем рендерер
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0xffffff);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container = document.getElementById('MyWebGLApp');
    container.appendChild(renderer.domElement);

    // Плоскость
    var planeMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x9999ff });
    var planGeo = new THREE.PlaneGeometry(170, 220, 17, 22);
    var plane = new THREE.Mesh(planGeo, planeMaterial);


    // create a rounded rectangle shape
    var roundedRectShape = new THREE.Shape();
    roundedRectShape.moveTo(10, 40);
    roundedRectShape.lineTo(10, 200);
    roundedRectShape.quadraticCurveTo(10, 210, 20, 210);
    roundedRectShape.lineTo(80, 210);
    roundedRectShape.lineTo(80, 170);
    roundedRectShape.lineTo(100, 170);
    roundedRectShape.lineTo(100, 210);
    roundedRectShape.lineTo(120, 210);
    roundedRectShape.lineTo(120, 170);
    roundedRectShape.lineTo(140, 170);
    roundedRectShape.lineTo(140, 210);
    roundedRectShape.lineTo(150, 210);
    roundedRectShape.lineTo(150, 80);
    roundedRectShape.lineTo(140, 80);
    roundedRectShape.lineTo(140, 10);
    roundedRectShape.lineTo(120, 10);
    roundedRectShape.lineTo(120, 30);
    roundedRectShape.lineTo(80, 30);
    roundedRectShape.lineTo(80, 10);
    roundedRectShape.lineTo(40, 10);
    roundedRectShape.quadraticCurveTo(10, 10, 10, 40);

    // create a geometry from the shape
    var roundedRectGeometry = new THREE.ShapeGeometry(roundedRectShape);
    var circleGeometry = new THREE.CircleGeometry(20, 360);
    var rectGeometry = new THREE.BoxGeometry(100, 30, 0);

    var roundedRectEdges = new THREE.EdgesGeometry(roundedRectGeometry);
    var circleEdges = new THREE.EdgesGeometry(circleGeometry);
    var rectEdges = new THREE.EdgesGeometry(rectGeometry);

    var strokeMaterial = new THREE.LineBasicMaterial({ color: 0x000000, linewidth: 2 });

    var roundedRectLine = new THREE.LineSegments(roundedRectEdges, strokeMaterial);
    var circleLine = new THREE.LineSegments(circleEdges, strokeMaterial);
    var rectLine = new THREE.LineSegments(rectEdges, strokeMaterial);
   
    plane.position.set(85, 110, 0);
    circleLine.position.set(40, 40, 0);
    rectLine.position.set(80, 125, 0);

    scene.add(plane);
    scene.add(roundedRectLine);
    scene.add(circleLine);
    scene.add(rectLine);
    // add the mesh to the scene
    // scene.add(roundedRectMesh);

}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    controls.update();
    renderer.render(scene, camera);
}

function AddCamera(X, Y, Z) {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.set(X, Y, Z);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 2;
    controls.noZoom = false;
    controls.zoomSpeed = 1.2;
    controls.staticMoving = true;
}

function AddLight(X, Y, Z) {
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(X, Y, Z);
    scene.add(light);
}
